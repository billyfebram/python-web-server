# Import socket module
import socket
import sys

def Main():
    # mengambil konfigurasi nama file yang akan diakses, host, dan port
    file = sys.argv[3]
    host = sys.argv[1]
    port = int(sys.argv[2])
    # inisialisasi socket
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    # koneksi ke socket dengan host dan port yg diinginkan
    s.connect((host,port))
    message = file
    while True:
        # setelah terkoneksi, langsung kirim nama file yg diinginkan ke server
        s.send(message.encode('ascii'))
        # menunggu server membalas permintaan klien dengan mengirimkan kembali konten file html yg diinginkan
        data = s.recv(4096)
        # print response dari server ke console
        print(data)
        break
    # menutup koneksi socket
    s.close()

if __name__ == '__main__':
    # jalankan method main
	Main()

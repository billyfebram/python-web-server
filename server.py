# import socket programming library
import socket

# import thread module
from thread import *
import threading

# inisialisasi object lock thread, untuk menghindari race condition
print_lock = threading.Lock()

# thread function
def threaded(c):
    while True:
        try:
            # menunggu menerima pesan dari client
            message = c.recv(4098)
            # jika tidak ada message, release thread lock
            if not message:
                print_lock.release()
                break

            # akses file yang direquest oleh user
            filename = '/'+message
            # membuka file
            f = open(filename[1:])
            # membaca file
            outputdata = f.read()
            # menutup file
            f.close()

            # mengirim response keuser berupa file html yg diminta
            c.send('HTTP/1.0 200 OK\n')
            c.send('Content-Type: text/html\n')
            c.send('\n')
            c.send(outputdata)
        except IOError:
            # apabila file yg direquest tidak ditemukan
            c.send('HTTP/1.1 404 Not Found')
    c.close()

# inisialisasi host dan port untuk koneksi socket yang akan dibuka oleh server
host = "localhost"
port = 12345

# inisialisasi socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
s.listen(5)

while True:
    # menunggu client terkoneksi ke socket
    c, addr = s.accept()
    # membuka kunci thread
    print_lock.acquire()
    # menjalankan thread baru
    start_new_thread(threaded, (c,))
# menutup socket
s.close()
